
from utils import tmp, do, collectLines, printLines, readInt, getListOfFiles

# https://linuxize.com/post/how-to-install-apache-maven-on-ubuntu-18-04/

# project = '/home/tl/tmp/FromMoodlev0/webapp-ejb/'


def makeEar(folder):
    mvnClean(folder)
    mvnCompile(folder)
    mvnInstall(folder)
    earfile = list(filter(lambda f: f.split('.')[-1] == 'ear',
                          getListOfFiles(folder)))
    return earfile[0]

def makeWar(folder):
    mvnClean(folder)
    mvnCompile(folder)
    mvnInstall(folder)
    warfile = list(filter(lambda f: f.split('.')[-1] == 'war',getListOfFiles(folder)))
    return warfile[0]


def mvnCompile(folder):
    last = folder.split('/')[-2]
    do(['cd ' + folder,
        'mvn compile > ' + tmp + last + '-compile.out',
        'echo $? > ' + tmp + last + '-compilestat.out'])
    printLines(collectLines(tmp + last + '-compile.out'))
    return readInt(tmp + last + '-compilestat.out')


def mvnClean(folder):
    last = folder.split('/')[-2]
    do(['cd ' + folder,
        'mvn clean > ' + tmp + last + '-clean.out',
        'echo $? > ' + tmp + last + '-cleanstat.out'])
    printLines(collectLines(tmp + last + '-clean.out'))
    return readInt(tmp + last + '-cleanstat.out')


def mvnInstall(folder):
    last = folder.split('/')[-2]
    do(['cd ' + folder,
        'mvn install > ' + tmp + last + '-install.out',
        'echo $? > ' + tmp + last + '-installstat.out'])
    printLines(collectLines(tmp + last + '-install.out'))
    return readInt(tmp + last + '-installstat.out')

