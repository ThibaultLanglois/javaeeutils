
import os
import stat

tmp = "/tmp/"


def do(commands):
    script = tmp + 'script.sh'
    f = open(script, 'w')
    for c in commands:
        f.write(c+'\n')
    f.close()
    os.chmod(script, stat.S_IRWXU)
    return os.system(script)


def collectLines(file):
    with open(file) as f:
        lines = f.readlines()
    return lines


def printLines(lines):
    for l in lines:
        print(l, end='')


def readInt(file):
    with open(file) as f:
        x = f.read()
    return int(x)


def getListOfFiles(folder):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(folder)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(folder, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)                
    return allFiles
