from utils import tmp, do, collectLines, printLines, readInt, getListOfFiles

# Change this line:
# wildflyFolder = '/home/tl/fcul/CSS/2019-2020/gitrepo4lab2/css-lab-git2/wildfly/wildfly-18.0.1.Final/'
wildflyFolder = '/home/tl/MyWorkspace/libraries/wildfly-21.0.1.Final/'

#------------------------------------------------------------------------------
wildflyBinFolder = wildflyFolder + 'bin/'
wildFlyDeploymentFolder = wildflyFolder + 'sandalone/deployments/'


def wildflyStart():
    do([wildflyBinFolder + 'standalone.sh > ' + tmp + 'wildfly.log &'])


def wildflyStop():
    do([wildflyBinFolder + '/jboss-cli.sh --connect --command="shutdown"'])


def wildflyDeploy(warOrEar):
    deployUsingWildFlyCli(warOrEar)


def deployUsingWildFlyCli(warOrEar):
    do([wildflyBinFolder + 'jboss-cli.sh --connect --command="deploy --force '
        + warOrEar + '"'])


def deployUsingCopy(warOrEar):
    do(['cp ' + warOrEar + ' ' + wildFlyDeploymentFolder])


def wildflyUndeploy(warOrEar):
    if warOrEar[0] == '/':
        print('You must provide the name of the war/ear, not the full path.')
        return
    do([wildflyBinFolder + 'jboss-cli.sh --connect --command="undeploy '
        + warOrEar + '"'])
