# JavaEEUtils

Import the `css` module that aggregates functions from the `maven` and the
`wildfly` modules. Set the `wildflyFolder` variable in the
`wildfly`module to point to the folder were wilfly is installed. After
wilfly installation you must give the execute permission to the shell
files:
```
cd where/wildfly/is/installed/
find bin/ -name '*.sh' -exec chmod a+x {} ';'
```

* To start wildfly:

```
     In [28]: wildflyStart()
```

After starting wildfly you can see the log openning a terminal
and running: 

```
    tail -f /tmp/wildfly.log
```

* Set a variable to the project folder:

     ```
     In [29]: myproject = '/home/tl/tmp/FromMoodlev0/webapp-ejb/'
     ```
 
* The `makeEar` function calls `mvnClean`, `mvnCompile` and `mvnInstall` to
create an ear file:

```
     In [30]: myear = makeEar(myproject)
```

Output of `mvnClean`:

```
     [INFO] Scanning for projects...
     [INFO] ------------------------------------------------------------------------
     [INFO] Reactor Build Order:
     [INFO] 
     [INFO] webapp-ejb                                                         [pom]
     [INFO] webapp-ejb-business                                                [ejb]
     [INFO] webapp-ejb-web-client                                              [war]
     [INFO] webapp-ejb-ear                                                     [ear]
     [INFO] 
     [INFO] -----------------< pt.ulisboa.ciencias.di:webapp-ejb >------------------
     [INFO] Building webapp-ejb 1.0                                            [1/4]
     [INFO] --------------------------------[ pom ]---------------------------------
     [INFO] 
     [INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ webapp-ejb ---
     [INFO] 
     [INFO] -------------< pt.ulisboa.ciencias.di:webapp-ejb-business >-------------
     [INFO] Building webapp-ejb-business 1.0                                   [2/4]
     [INFO] --------------------------------[ ejb ]---------------------------------
     [INFO] 
     [INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ webapp-ejb-business ---
     [INFO] Deleting /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/target
     [INFO] 
     [INFO] ------------< pt.ulisboa.ciencias.di:webapp-ejb-web-client >------------
     [INFO] Building webapp-ejb-web-client 1.0                                 [3/4]
     [INFO] --------------------------------[ war ]---------------------------------
     [INFO] 
     [INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ webapp-ejb-web-client ---
     [INFO] Deleting /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/target
     [INFO] 
     [INFO] ---------------< pt.ulisboa.ciencias.di:webapp-ejb-ear >----------------
     [INFO] Building webapp-ejb-ear 1.0                                        [4/4]
     [INFO] --------------------------------[ ear ]---------------------------------
     [INFO] 
     [INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ webapp-ejb-ear ---
     [INFO] Deleting /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/target
     [INFO] ------------------------------------------------------------------------
     [INFO] Reactor Summary for webapp-ejb 1.0:
     [INFO] 
     [INFO] webapp-ejb ......................................... SUCCESS [  0.061 s]
     [INFO] webapp-ejb-business ................................ SUCCESS [  0.008 s]
     [INFO] webapp-ejb-web-client .............................. SUCCESS [  0.009 s]
     [INFO] webapp-ejb-ear ..................................... SUCCESS [  0.023 s]
     [INFO] ------------------------------------------------------------------------
     [INFO] BUILD SUCCESS
     [INFO] ------------------------------------------------------------------------
     [INFO] Total time:  0.591 s
     [INFO] Finished at: 2019-12-07T12:11:04Z
```

Output of `mvnCompile`:

```
     [INFO] ------------------------------------------------------------------------
     [INFO] Scanning for projects...
     [INFO] ------------------------------------------------------------------------
     [INFO] Reactor Build Order:
     [INFO] 
     [INFO] webapp-ejb                                                         [pom]
     [INFO] webapp-ejb-business                                                [ejb]
     [INFO] webapp-ejb-web-client                                              [war]
     [INFO] webapp-ejb-ear                                                     [ear]
     [INFO] 
     [INFO] -----------------< pt.ulisboa.ciencias.di:webapp-ejb >------------------
     [INFO] Building webapp-ejb 1.0                                            [1/4]
     [INFO] --------------------------------[ pom ]---------------------------------
     [INFO] 
     [INFO] -------------< pt.ulisboa.ciencias.di:webapp-ejb-business >-------------
     [INFO] Building webapp-ejb-business 1.0                                   [2/4]
     [INFO] --------------------------------[ ejb ]---------------------------------
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ webapp-ejb-business ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] Copying 3 resources
     [INFO] 
     [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ webapp-ejb-business ---
     [INFO] Changes detected - recompiling the module!
     [INFO] Compiling 30 source files to /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/target/classes
     [INFO] 
     [INFO] ------------< pt.ulisboa.ciencias.di:webapp-ejb-web-client >------------
     [INFO] Building webapp-ejb-web-client 1.0                                 [3/4]
     [INFO] --------------------------------[ war ]---------------------------------
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ webapp-ejb-web-client ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] Copying 1 resource
     [INFO] 
     [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ webapp-ejb-web-client ---
     [INFO] Changes detected - recompiling the module!
     [INFO] Compiling 12 source files to /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/target/classes
     [INFO] 
     [INFO] ---------------< pt.ulisboa.ciencias.di:webapp-ejb-ear >----------------
     [INFO] Building webapp-ejb-ear 1.0                                        [4/4]
     [INFO] --------------------------------[ ear ]---------------------------------
     [INFO] 
     [INFO] --- maven-ear-plugin:2.6:generate-application-xml (default-generate-application-xml) @ webapp-ejb-ear ---
     [INFO] Generating application.xml
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ webapp-ejb-ear ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] skip non existing resourceDirectory /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/src/main/resources
     [INFO] ------------------------------------------------------------------------
     [INFO] Reactor Summary for webapp-ejb 1.0:
     [INFO] 
     [INFO] webapp-ejb ......................................... SUCCESS [  0.003 s]
     [INFO] webapp-ejb-business ................................ SUCCESS [  0.930 s]
     [INFO] webapp-ejb-web-client .............................. SUCCESS [  0.117 s]
     [INFO] webapp-ejb-ear ..................................... SUCCESS [  0.176 s]
     [INFO] ------------------------------------------------------------------------
     [INFO] BUILD SUCCESS
     [INFO] ------------------------------------------------------------------------
     [INFO] Total time:  1.698 s
     [INFO] Finished at: 2019-12-07T12:11:07Z
     [INFO] ------------------------------------------------------------------------
```

Output of `mvnInstall`:

```
     [INFO] Scanning for projects...
     [INFO] ------------------------------------------------------------------------
     [INFO] Reactor Build Order:
     [INFO] 
     [INFO] webapp-ejb                                                         [pom]
     [INFO] webapp-ejb-business                                                [ejb]
     [INFO] webapp-ejb-web-client                                              [war]
     [INFO] webapp-ejb-ear                                                     [ear]
     [INFO] 
     [INFO] -----------------< pt.ulisboa.ciencias.di:webapp-ejb >------------------
     [INFO] Building webapp-ejb 1.0                                            [1/4]
     [INFO] --------------------------------[ pom ]---------------------------------
     [INFO] 
     [INFO] --- maven-install-plugin:2.4:install (default-install) @ webapp-ejb ---
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/pom.xml to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb/1.0/webapp-ejb-1.0.pom
     [INFO] 
     [INFO] -------------< pt.ulisboa.ciencias.di:webapp-ejb-business >-------------
     [INFO] Building webapp-ejb-business 1.0                                   [2/4]
     [INFO] --------------------------------[ ejb ]---------------------------------
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ webapp-ejb-business ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] Copying 3 resources
     [INFO] 
     [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ webapp-ejb-business ---
     [INFO] Nothing to compile - all classes are up to date
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ webapp-ejb-business ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] skip non existing resourceDirectory /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/src/test/resources
     [INFO] 
     [INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ webapp-ejb-business ---
     [INFO] No sources to compile
     [INFO] 
     [INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ webapp-ejb-business ---
     [INFO] No tests to run.
     [INFO] 
     [INFO] --- maven-ejb-plugin:2.3:ejb (default-ejb) @ webapp-ejb-business ---
     [INFO] Building EJB webapp-ejb-business with EJB version 3.1
     [INFO] Building jar: /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/target/webapp-ejb-business.jar
     [INFO] Building EJB client webapp-ejb-business-client
     [INFO] Building jar: /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/target/webapp-ejb-business-client.jar
     [INFO] 
     [INFO] --- maven-install-plugin:2.4:install (default-install) @ webapp-ejb-business ---
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/target/webapp-ejb-business.jar to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-business/1.0/webapp-ejb-business-1.0.jar
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/pom.xml to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-business/1.0/webapp-ejb-business-1.0.pom
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-business/target/webapp-ejb-business-client.jar to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-business/1.0/webapp-ejb-business-1.0-client.jar
     [INFO] 
     [INFO] ------------< pt.ulisboa.ciencias.di:webapp-ejb-web-client >------------
     [INFO] Building webapp-ejb-web-client 1.0                                 [3/4]
     [INFO] --------------------------------[ war ]---------------------------------
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ webapp-ejb-web-client ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] Copying 1 resource
     [INFO] 
     [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ webapp-ejb-web-client ---
     [INFO] Nothing to compile - all classes are up to date
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ webapp-ejb-web-client ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] skip non existing resourceDirectory /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/src/test/resources
     [INFO] 
     [INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ webapp-ejb-web-client ---
     [INFO] No sources to compile
     [INFO] 
     [INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ webapp-ejb-web-client ---
     [INFO] No tests to run.
     [INFO] 
     [INFO] --- maven-war-plugin:2.1.1:war (default-war) @ webapp-ejb-web-client ---
     [INFO] Packaging webapp
     [INFO] Assembling webapp [webapp-ejb-web-client] in [/home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/target/webapp-ejb-web-client]
     [INFO] Processing war project
     [INFO] Copying webapp resources [/home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/src/main/webapp]
     [INFO] Webapp assembled in [22 msecs]
     [INFO] Building war: /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/target/webapp-ejb-web-client.war
     [INFO] 
     [INFO] --- maven-install-plugin:2.4:install (default-install) @ webapp-ejb-web-client ---
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/target/webapp-ejb-web-client.war to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-web-client/1.0/webapp-ejb-web-client-1.0.war
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-web-client/pom.xml to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-web-client/1.0/webapp-ejb-web-client-1.0.pom
     [INFO] 
     [INFO] ---------------< pt.ulisboa.ciencias.di:webapp-ejb-ear >----------------
     [INFO] Building webapp-ejb-ear 1.0                                        [4/4]
     [INFO] --------------------------------[ ear ]---------------------------------
     [INFO] 
     [INFO] --- maven-ear-plugin:2.6:generate-application-xml (default-generate-application-xml) @ webapp-ejb-ear ---
     [INFO] Generating application.xml
     [INFO] 
     [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ webapp-ejb-ear ---
     [INFO] Using 'UTF-8' encoding to copy filtered resources.
     [INFO] skip non existing resourceDirectory /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/src/main/resources
     [INFO] 
     [INFO] --- maven-ear-plugin:2.6:ear (default-ear) @ webapp-ejb-ear ---
     [INFO] Copying artifact[war:pt.ulisboa.ciencias.di:webapp-ejb-web-client:1.0] to[webapp-ejb-web-client.war]
     [INFO] Copying artifact[ejb:pt.ulisboa.ciencias.di:webapp-ejb-business:1.0] to[webapp-ejb-business.jar]
     [INFO] Copying artifact[jar:mysql:mysql-connector-java:5.1.38] to[lib/mysql-connector-java.jar]
     [INFO] Copy ear sources to /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/target/webapp-ejb-ear-1.0
     [INFO] Could not find manifest file: /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/target/webapp-ejb-ear-1.0/META-INF/MANIFEST.MF - Generating one
     [INFO] Building jar: /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/target/webapp-ejb-ear-1.0.ear
     [INFO] 
     [INFO] --- maven-install-plugin:2.4:install (default-install) @ webapp-ejb-ear ---
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/target/webapp-ejb-ear-1.0.ear to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-ear/1.0/webapp-ejb-ear-1.0.ear
     [INFO] Installing /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/pom.xml to /home/tl/.m2/repository/pt/ulisboa/ciencias/di/webapp-ejb-ear/1.0/webapp-ejb-ear-1.0.pom
     [INFO] ------------------------------------------------------------------------
     [INFO] Reactor Summary for webapp-ejb 1.0:
     [INFO] 
     [INFO] webapp-ejb ......................................... SUCCESS [  0.113 s]
     [INFO] webapp-ejb-business ................................ SUCCESS [  0.790 s]
     [INFO] webapp-ejb-web-client .............................. SUCCESS [  0.301 s]
     [INFO] webapp-ejb-ear ..................................... SUCCESS [  0.316 s]
     [INFO] ------------------------------------------------------------------------
     [INFO] BUILD SUCCESS
     [INFO] ------------------------------------------------------------------------
     [INFO] Total time:  1.994 s
     [INFO] Finished at: 2019-12-07T12:11:10Z
     [INFO] ------------------------------------------------------------------------
```

* The ear file is:

```
     In [31]: print(myear)
     /home/tl/tmp/FromMoodlev0/webapp-ejb/webapp-ejb-ear/target/webapp-ejb-ear-1.0.ear
```

* Deploy the application:

```
   In [32]: wildflyDeploy(myear)
```

* Test the app in the browser.

* Undeploy the application:

```
  In [33]: wildflyUndeploy('webapp-ejb-ear-1.0.ear')

```

* Stop wilfly:

```
  In [34]: wildflyStop()
```
